#include <Arduino.h>
#include <WiFi.h>
#include <ArduinoJson.h>
#include <stdlib.h>

#define SSID "www"
#define SSID_PASSWOD "12345678"

#define SERVER_PORT 9999

WiFiServer wifiServer(SERVER_PORT);


char response[255];
char packetBuffer[255]; // buffer to hold incoming packet

void setup()
{
	srand(0);
	Serial.begin(115200);
	// Connect to WiFi
	WiFi.begin(SSID, SSID_PASSWOD);
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");
	}
	Serial.println("WiFi connected");
	// Print the IP address
	Serial.println(WiFi.localIP());

	wifiServer.begin();
}

void loop()
{
	
	StaticJsonDocument<200> docReq;

	WiFiClient client = wifiServer.available();

	if (client)
	{

		while (client.connected())
		{
			while (client.available() > 0)
			{
				StaticJsonDocument<200> docResp;
				String x = client.readStringUntil('\n');
				if (x.equals("gps"))
				{
					docResp["status"] = 200;
					docResp["sensor"] = "gps";
					docResp["time"] = millis();

					JsonArray data = docResp.createNestedArray("data");
					data.add(rand() % 100);
					data.add(rand() % 100);
				}
				else
				{
					docResp["status"] = 400;
				}
				serializeJson(docResp, response, sizeof(response));
				client.write(response);
			}

			/*
			while (client.available() > 0)
			{
				char c = client.read();
				client.write(c);
			}
			*/

			delay(10);
		}

		client.stop();
		Serial.println("Client disconnected");
	}

}