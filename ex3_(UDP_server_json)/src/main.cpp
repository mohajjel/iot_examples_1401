#include <Arduino.h>
#include <WiFi.h>
#include <ArduinoJson.h>
#include <stdlib.h>

#define SSID "DESKTOP-BBBO6RP 0758"
#define SSID_PASSWOD "12345678"

WiFiUDP Udp;

String a;
char response[255];
char packetBuffer[255]; // buffer to hold incoming packet

void setup()
{
	srand(0);
	Serial.begin(115200);
	// Connect to WiFi
	WiFi.begin(SSID, SSID_PASSWOD);
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");
	}
	Serial.println("WiFi connected");
	// Print the IP address
	Serial.println(WiFi.localIP());

	Udp.begin(9999);
}

void loop()
{
	StaticJsonDocument<200> docResp;
	StaticJsonDocument<200> docReq;

	int packetSize = Udp.parsePacket();

	if (packetSize)
	{

		Serial.print("Received packet of size ");

		Serial.println(packetSize);

		Serial.print("From ");

		IPAddress remoteIp = Udp.remoteIP();

		Serial.print(remoteIp);

		Serial.print(", port ");

		Serial.println(Udp.remotePort());

		// read the packet into packetBufffer

		int len = Udp.read(packetBuffer, 255);

		if (len > 0)
		{

			packetBuffer[len] = 0;
		}

		Serial.println("Contents:");
		Serial.println(packetBuffer);

		// Deserialize the JSON document
		DeserializationError error = deserializeJson(docReq, packetBuffer);

		// Test if parsing succeeds.
		if (error)
		{
			Serial.print(F("deserializeJson() failed: "));
			Serial.println(error.f_str());
			return;
		}

		// send a reply, to the IP address and port that sent us the packet we received

		const char *sensor = docReq["sensor"];
		if (docReq.containsKey("sensor") && strcmp(sensor, "gps") == 0)
		{
			docResp["status"] = 200;
			docResp["sensor"] = "gps";
			docResp["time"] = millis();

			JsonArray data = docResp.createNestedArray("data");
			data.add(rand() % 100);
			data.add(rand() % 100);
		}
		else
		{
			docResp["status"] = 400;
		}
		serializeJson(docResp, response, sizeof(response));
		Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
		Udp.write((uint8_t *)response, strlen(response));
		Udp.endPacket();
	}
}