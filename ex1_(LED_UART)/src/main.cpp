#include <Arduino.h>

int ledPin1 = 5; // LED_BUILTIN; // The pin that the LED is connected to
int ledPin2 = 6; // LED_BUILTIN; // The pin that the LED is connected to
int ledPin3 = 7; // LED_BUILTIN; // The pin that the LED is connected to

void setup()
{
  pinMode(ledPin1, OUTPUT); // Set the LED pin as an output
  pinMode(ledPin2, OUTPUT); // Set the LED pin as an output
  pinMode(ledPin3, OUTPUT); // Set the LED pin as an output
  
  Serial.begin(9600);
}

void loop()
{
  digitalWrite(ledPin1, HIGH);
  //digitalWrite(ledPin2, HIGH);
  digitalWrite(ledPin3, HIGH);
  
  Serial.println("Hello, world!"); // print the string

  delay(1000);                     // wait for one second
  digitalWrite(ledPin1, LOW);
  //digitalWrite(ledPin2, LOW);
  digitalWrite(ledPin3, LOW);
  delay(1000);
}
