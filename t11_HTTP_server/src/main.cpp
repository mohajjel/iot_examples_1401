#include <Arduino.h>
#include <WiFi.h>
#include <ArduinoJson.h>
#include <stdlib.h>
#include <WebServer.h>

#define SSID "www"
#define SSID_PASSWOD "12345678"

String a;
char response[255];
char packetBuffer[255]; // buffer to hold incoming packet
WebServer server(30000);
void handleRoot();
void handleNotFound();
void setup()
{
	pinMode(LED_BUILTIN, OUTPUT);
	Serial.begin(115200);
	// Connect to WiFi
	WiFi.begin(SSID, SSID_PASSWOD);
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");
	}
	Serial.println("WiFi connected");
	// Print the IP address
	Serial.println(WiFi.localIP());

	server.on(F("/"), handleRoot);

	server.on(F("/inline"), []()
			  { server.send(200, F("text/plain"), F("This works as well")); });
	server.on(F("/on"), []()
			  { digitalWrite(LED_BUILTIN, HIGH); server.send(200, F("text/plain"), F("LED ON")); });
	server.on(F("/off"), []()
			  { digitalWrite(LED_BUILTIN, LOW); server.send(200, F("text/plain"), F("LED OFF")); });
	
	server.onNotFound(handleNotFound);

	server.begin();

	Serial.print(F("HTTP server started @ "));
	Serial.println(WiFi.localIP());
}

void loop()
{
	server.handleClient();
}

void handleRoot()
{
#define BUFFER_SIZE 512

	// digitalWrite(LED_BUILTIN, 1);
	char temp[BUFFER_SIZE];
	int sec = millis() / 1000;
	int min = sec / 60;
	int hr = min / 60;
	int day = hr / 24;

	snprintf(temp, BUFFER_SIZE - 1,
			 "<html>\
<head>\
<meta http-equiv='refresh' content='5'/>\
<title>ESP32</title>\
<style>\
body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
</style>\
</head>\
<body>\
<h1>Hello from ESP32</h1>\
<h3>running WiFiWebServer</h3>\
<h3>on ESP32</h3>\
<p>Uptime: %d d %02d:%02d:%02d</p>\
</body>\
</html>",
			 day, hr, min % 60, sec % 60);

	server.send(200, F("text/html"), temp);
	// digitalWrite(LED_BUILTIN, 0);
}

void handleNotFound()
{
	// digitalWrite(LED_BUILTIN, 1);

	String message = F("File Not Found\n\n");

	message += F("URI: ");
	message += server.uri();
	message += F("\nMethod: ");
	message += (server.method() == HTTP_GET) ? F("GET") : F("POST");
	message += F("\nArguments: ");
	message += server.args();
	message += F("\n");

	for (uint8_t i = 0; i < server.args(); i++)
	{
		message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
	}

	server.send(404, F("text/plain"), message);

	// digitalWrite(LED_BUILTIN, 0);
}
