#include <Arduino.h>
#include <WiFi.h>
#include <ArduinoJson.h>
#include <stdlib.h>

#define SSID "www"
#define SSID_PASSWOD "12345678"

#define SERVER_PORT 30000
#define SERVER_IP "192.168.43.240"

WiFiClient client;

char response[255];
char packetBuffer[255]; // buffer to hold incoming packet

void setup()
{
	srand(0);
	Serial.begin(115200);
	// Connect to WiFi
	WiFi.begin(SSID, SSID_PASSWOD);
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");
	}
	Serial.println("WiFi connected");
	// Print the IP address
	Serial.println(WiFi.localIP());

	if (client.connect(SERVER_IP, SERVER_PORT))
	{
		Serial.println("Connected to server");
	}
	else
	{
		Serial.println("Connection failed");
	}
}

void loop()
{

	// StaticJsonDocument<200> docReq;

	// WiFiClient client = wifiServer.available();

	if (client.connected())
	{
		// Send a message to the server
		client.println("Hello, world!");

		// Wait for response from the server
		while (client.available() == 0)
		{
		}
		while (client.available())
		{
			char c = client.read();
			Serial.print(c);
		}
	}
	else
	{
		Serial.println("Disconnected from server");
		client.stop();

		// Reconnect to the server
		if (client.connect(SERVER_IP, SERVER_PORT))
		{
			Serial.println("Connected to server");
		}
		else
		{
			Serial.println("Connection failed");
		}
	}

	delay(1000);
}