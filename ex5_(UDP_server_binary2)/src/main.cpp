#include <Arduino.h>
#include <WiFi.h>

#define SSID "DESKTOP-BBBO6RP 0758"
#define SSID_PASSWOD "12345678"

String a;

WiFiUDP Udp;
typedef struct Data_t
{
	int32_t x;
	int16_t y;
} Data_t;

char packetBuffer[255];
void setup()
{

	Serial.begin(115200);
	// Connect to WiFi
	WiFi.begin(SSID, SSID_PASSWOD);
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");
	}
	Serial.println("WiFi connected");
	// Print the IP address
	Serial.println(WiFi.localIP());

	Udp.begin(30000);
}

void loop()
{
	// if there's data available, read a packet

	int packetSize = Udp.parsePacket();

	if (packetSize)
	{

		Serial.print("Received packet of size ");

		Serial.println(packetSize);

		Serial.print("From ");

		IPAddress remoteIp = Udp.remoteIP();

		Serial.print(remoteIp);

		Serial.print(", port ");

		Serial.println(Udp.remotePort());

		// read the packet into packetBufffer

		int len = Udp.read(packetBuffer, 255);

		if (len > 0)
		{

			packetBuffer[len] = 0;
		}

		Serial.println("Contents:");

		Serial.println(packetBuffer);

		if (len == 6)
		{
			Data_t tmp = *((Data_t *)packetBuffer);
			Serial.printf("x=%x, y=%x\r\n", tmp.x, tmp.y);
			Data_t data = {tmp.x - tmp.y, tmp.x + tmp.y};
			Serial.printf("x=%x, y=%x\r\n", data.x, data.y);

			Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());

			Udp.write((uint8_t *)&data, sizeof(data));

			Udp.endPacket();
		}
	}
}