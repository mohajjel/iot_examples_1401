#include <Arduino.h>
#include <WiFi.h>

#define SSID "DESKTOP-BBBO6RP 0758"
#define SSID_PASSWOD "12345678"

String a;

WiFiUDP Udp;
typedef struct Data_t
{
	uint32_t time;
	int32_t x;
	int32_t y;
} Data_t;

char packetBuffer[255];
void setup()
{

	Serial.begin(115200);
	// Connect to WiFi
	WiFi.begin(SSID, SSID_PASSWOD);
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");
	}
	Serial.println("WiFi connected");
	// Print the IP address
	Serial.println(WiFi.localIP());

	Udp.begin(30000);
}

void loop()
{
	// if there's data available, read a packet

	int packetSize = Udp.parsePacket();

	if (packetSize)
	{

		Serial.print("Received packet of size ");

		Serial.println(packetSize);

		Serial.print("From ");

		IPAddress remoteIp = Udp.remoteIP();

		Serial.print(remoteIp);

		Serial.print(", port ");

		Serial.println(Udp.remotePort());

		// read the packet into packetBufffer

		int len = Udp.read(packetBuffer, 255);

		if (len > 0)
		{

			packetBuffer[len] = 0;
		}

		Serial.println("Contents:");

		Serial.println(packetBuffer);

		if (strcmp(packetBuffer, "gps") == 0)
		{
			Data_t data = {millis(), 1, 2};
			Serial.printf("time=%u, x=%d, y=%d\r\n",data.time, data.x, data.y);

			Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());

			Udp.write((uint8_t *)&data, sizeof(data));

			Udp.endPacket();
		}
	}
}