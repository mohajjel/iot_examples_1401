#include <Arduino.h>
#include <WiFi.h>


#define SSID "DESKTOP-BBBO6RP 0758"
#define SSID_PASSWOD "12345678"

int ledPin1 = 5; // LED_BUILTIN; // The pin that the LED is connected to
int ledPin2 = 6; // LED_BUILTIN; // The pin that the LED is connected to
int ledPin3 = 7; // LED_BUILTIN; // The pin that the LED is connected to

String a;


WiFiUDP Udp;

char packetBuffer[255];				 // buffer to hold incoming packet
char ReplyBuffer[] = "acknowledged"; // a string to send back

void setup()
{
	pinMode(ledPin1, OUTPUT); // Set the LED pin as an output
	pinMode(ledPin2, OUTPUT); // Set the LED pin as an output
	pinMode(ledPin3, OUTPUT); // Set the LED pin as an output
	Serial.begin(115200);
	// Connect to WiFi
	WiFi.begin(SSID, SSID_PASSWOD);
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");
	}
	Serial.println("WiFi connected");
	// Print the IP address
	Serial.println(WiFi.localIP());
	Udp.begin(30000);
}

void loop()
{
	// if there's data available, read a packet

	int packetSize = Udp.parsePacket();

	if (packetSize)
	{

		Serial.print("Received packet of size ");

		Serial.println(packetSize);

		Serial.print("From ");

		IPAddress remoteIp = Udp.remoteIP();

		Serial.print(remoteIp);

		Serial.print(", port ");

		Serial.println(Udp.remotePort());

		// read the packet into packetBufffer

		int len = Udp.read(packetBuffer, 255);

		if (len > 0)
		{

			packetBuffer[len] = 0;
		}
		Udp.flush();
		Serial.println("Contents:");

		Serial.println(packetBuffer);
		if (strcmp(packetBuffer, "1") == 0)
		{
			digitalWrite(ledPin1, HIGH);
		}
		else{
			digitalWrite(ledPin1, LOW);
		}

		// send a reply, to the IP address and port that sent us the packet we received

		Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());

		Udp.write((uint8_t *)ReplyBuffer, sizeof(ReplyBuffer));

		Udp.endPacket();
	}
}