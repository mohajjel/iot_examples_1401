#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>

#define SSID "www"
#define SSID_PASSWOD "12345678"

int ledPin1 = 5; // LED_BUILTIN; // The pin that the LED is connected to
int ledPin2 = 6; // LED_BUILTIN; // The pin that the LED is connected to
int ledPin3 = 7; // LED_BUILTIN; // The pin that the LED is connected to

String a;

AsyncUDP Udp;

char packetBuffer[255];				 // buffer to hold incoming packet
char ReplyBuffer[] = "acknowledged"; // a string to send back

void onpacket_handler(AsyncUDPPacket packet)
{
	Serial.print("Received packet: ");
	Serial.write(packet.data(), packet.length());
	Udp.writeTo(packet.data(), packet.length(), packet.remoteIP(), packet.remotePort());
}

void setup()
{
	pinMode(ledPin1, OUTPUT); // Set the LED pin as an output
	pinMode(ledPin2, OUTPUT); // Set the LED pin as an output
	pinMode(ledPin3, OUTPUT); // Set the LED pin as an output
	Serial.begin(115200);
	// Connect to WiFi
	WiFi.begin(SSID, SSID_PASSWOD);
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");
	}
	Serial.println("WiFi connected");
	// Print the IP address
	Serial.println(WiFi.localIP());
	Udp.listen(30000);
	Udp.onPacket(onpacket_handler);
}

void loop()
{
	delay(1000);
}