#include <WiFi.h>
#include <WiFiUdp.h>
#include <coap-simple.h>
#include "SPIFFS.h"

const char *ssid = "www";
const char *password = "m33553502";

void test_SPIFFS()
{
	if (!SPIFFS.begin(true))
	{
		Serial.println("An Error has occurred while mounting SPIFFS");
		return;
	}

	File file = SPIFFS.open("/t1.txt");
	if (!file)
	{
		Serial.println("Failed to open file for reading");
		return;
	}

	Serial.println("File Content:");
	while (file.available())
	{
		Serial.write(file.read());
	}
	file.close();
}

void setup()
{
	pinMode(LED_BUILTIN, OUTPUT);
	Serial.begin(115200);

	WiFi.begin(ssid, password);
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");
	}

	Serial.println("");
	Serial.println("WiFi connected");
	Serial.println("IP address: ");
	Serial.println(WiFi.localIP());

	test_SPIFFS();
}

void loop()
{
}